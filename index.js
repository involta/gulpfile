module.exports = function (config) {

    // modules
    var async = require('async');
    var gulp = require('gulp');
    var $ = require('gulp-load-plugins')();

    // modify config
    modifyConfig(config, config.aliases);

    // site
    gulp.task('site:js', function () {
        return js(config.siteJsSrc, config.siteJsDist, config.siteJsConcat);
    });

    gulp.task('site:css', function () {
        return css(config.siteCssSrc, config.siteCssDist);
    });

    gulp.task('site', ['site:js', 'site:css']);

    // vendor
    gulp.task('vendor:js', function () {
        return js(config.vendorJsSrc, config.vendorJsDist, config.vendorJsConcat);
    });

    gulp.task('vendor:css', function () {
        return css(config.vendorCssSrc, config.vendorCssDist, config.vendorCssConcat);
    });

    gulp.task('vendor', ['vendor:js', 'vendor:css']);

    // watch
    gulp.task('watch:js', function () {
        return gulp.watch(config.watchJs, ['site:js']);
    });

    gulp.task('watch:css', function () {
        return gulp.watch(config.watchCss, ['site:css']);
    });

    gulp.task('watch', ['watch:js', 'watch:css']);

    // iconfont
    gulp.task('iconfont', function (done) {
        return makeIconFont({svg: config.iconFontSvgSrc, cssTemplate: config.iconFontCssTemplateSrc}, {font: config.iconFontDist, css: config.iconFontCssDist}, config.iconFontCfg, done);
    });

    // sort
    gulp.task('sort', function () {
        return sortCss(config.sortCssPath, config.sortCssExclusions)
    });

    // optimize
    gulp.task('optimize:css', function () {
        return optimizeCss(config.optimizeCssPath)
    });

    gulp.task('optimize:js', function () {
        return optimizeJs(config.optimizeJsPath)
    });

    gulp.task('optimize', ['optimize:js', 'optimize:css']);

    // compress
    gulp.task('compress:img', function () {
        return compressImg(config.compressImgSrc, config.compressImgDist)
    });

    gulp.task('compress', ['compress:img']);

    // livereload
    gulp.task('livereload', function () {
        $.livereload.listen();
        return gulp.watch(config.livereloadSrc, function (file) {
            $.livereload.changed(file);
        });
    });

    // build
    gulp.task('build', ['site', 'vendor']);

    // dev
    gulp.task('dev', $.sequence('build', 'watch', 'livereload'));

    // prod
    gulp.task('prod', $.sequence('build', 'optimize'));

    // common functions
    function compressImg(src, dist) {
        return gulp.src(src)
            .pipe($.imagemin())
            .pipe(gulp.dest(dist));
    }

    function js(src, dist, concat) {
        return gulp.src(src)
            .pipe($.plumber())
            .pipe($.if(!!concat, $.concat(concat || 'concat.js')))
            .pipe(gulp.dest(dist))
    }

    function css(src, dist, concat) {
        return gulp.src(src)
            .pipe($.sass().on("error", $.sass.logError))
            .pipe($.if(!!concat, $.concat(concat || 'concat.css')))
            .pipe(gulp.dest(dist));
    }

    function makeIconFont(src, dist, cfg, done) {

        var iconStream = gulp.src(src.svg + "/*.svg")
            .pipe($.iconfont(cfg.font));

        return async.parallel([
            function handleGlyphs(cb) {
                iconStream.on('glyphs', function (glyphs, options) {
                    gulp.src(src.cssTemplate + '/_' + cfg.font.fontName + '.scss')
                        .pipe($.consolidate('lodash', Object.assign({glyphs: glyphs}, cfg.css)))
                        .pipe(gulp.dest(dist.css))
                        .on('finish', cb);
                });
            },
            function handleFonts(cb) {
                iconStream
                    .pipe(gulp.dest(dist.font))
                    .on('finish', cb);
            }
        ], done);
    }

    function sortCss(path, exclusions) {

        var src = [
            path + '/**'
        ].concat(exclusions);

        // based on https://github.com/brigade/scss-lint/blob/master/data/property-sort-orders/smacss.txt (3dd8055, 29 Dec 2017)
        var sortingOrder = {
            "properties-order": [
                "content",
                "quotes",
                "display",
                "visibility",
                "position",
                "z-index",
                "top",
                "right",
                "bottom",
                "left",
                "box-sizing",
                "grid",
                "grid-after",
                "grid-area",
                "grid-auto-columns",
                "grid-auto-flow",
                "grid-auto-rows",
                "grid-before",
                "grid-column",
                "grid-column-end",
                "grid-column-gap",
                "grid-column-start",
                "grid-columns",
                "grid-end",
                "grid-gap",
                "grid-row",
                "grid-row-end",
                "grid-row-gap",
                "grid-row-start",
                "grid-rows",
                "grid-start",
                "grid-template",
                "grid-template-areas",
                "grid-template-columns",
                "grid-template-rows",
                "flex",
                "flex-basis",
                "flex-direction",
                "flex-flow",
                "flex-grow",
                "flex-shrink",
                "flex-wrap",
                "align-content",
                "align-items",
                "align-self",
                "justify-content",
                "order",
                "width",
                "min-width",
                "max-width",
                "height",
                "min-height",
                "max-height",
                "margin",
                "margin-top",
                "margin-right",
                "margin-bottom",
                "margin-left",
                "padding",
                "padding-top",
                "padding-right",
                "padding-bottom",
                "padding-left",
                "float",
                "clear",
                "overflow",
                "overflow-x",
                "overflow-y",
                "clip",
                "zoom",
                "columns",
                "column-gap",
                "column-fill",
                "column-rule",
                "column-span",
                "column-count",
                "column-width",
                "table-layout",
                "empty-cells",
                "caption-side",
                "border-spacing",
                "border-collapse",
                "list-style",
                "list-style-position",
                "list-style-type",
                "list-style-image",
                "transform",
                "transform-origin",
                "transform-style",
                "backface-visibility",
                "perspective",
                "perspective-origin",
                "transition",
                "transition-property",
                "transition-duration",
                "transition-timing-function",
                "transition-delay",
                "animation",
                "animation-name",
                "animation-duration",
                "animation-play-state",
                "animation-timing-function",
                "animation-delay",
                "animation-iteration-count",
                "animation-direction",
                "border",
                "border-top",
                "border-right",
                "border-bottom",
                "border-left",
                "border-width",
                "border-top-width",
                "border-right-width",
                "border-bottom-width",
                "border-left-width",
                "border-style",
                "border-top-style",
                "border-right-style",
                "border-bottom-style",
                "border-left-style",
                "border-radius",
                "border-top-left-radius",
                "border-top-right-radius",
                "border-bottom-left-radius",
                "border-bottom-right-radius",
                "border-color",
                "border-top-color",
                "border-right-color",
                "border-bottom-color",
                "border-left-color",
                "outline",
                "outline-color",
                "outline-offset",
                "outline-style",
                "outline-width",
                "stroke-width",
                "stroke-linecap",
                "stroke-dasharray",
                "stroke-dashoffset",
                "stroke",
                "opacity",
                "background",
                "background-color",
                "background-image",
                "background-repeat",
                "background-position",
                "background-size",
                "box-shadow",
                "fill",
                "color",
                "font",
                "font-family",
                "font-size",
                "font-size-adjust",
                "font-stretch",
                "font-effect",
                "font-style",
                "font-variant",
                "font-weight",
                "font-emphasize",
                "font-emphasize-position",
                "font-emphasize-style",
                "letter-spacing",
                "line-height",
                "list-style",
                "word-spacing",
                "text-align",
                "text-align-last",
                "text-decoration",
                "text-indent",
                "text-justify",
                "text-overflow",
                "text-overflow-ellipsis",
                "text-overflow-mode",
                "text-rendering",
                "text-outline",
                "text-shadow",
                "text-transform",
                "text-wrap",
                "word-wrap",
                "word-break",
                "text-emphasis",
                "text-emphasis-color",
                "text-emphasis-style",
                "text-emphasis-position",
                "vertical-align",
                "white-space",
                "word-spacing",
                "hyphens",
                "src",
                "tab-size",
                "counter-reset",
                "counter-increment",
                "resize",
                "cursor",
                "pointer-events",
                "speak",
                "user-select",
                "nav-index",
                "nav-up",
                "nav-right",
                "nav-down",
                "nav-left"
            ]
        };

        return gulp.src(src)
            .pipe($.postcss([
                require('postcss-sorting')(sortingOrder)
            ], {syntax: require('postcss-scss')}))
            .pipe(gulp.dest(path))
    }

    function optimizeJs(path) {
        var src = [
            '!' + path + '/*.min.js',
            path + '/*.js'
        ];
        return gulp.src(src)
            .pipe($.minify({
                ext: {
                    min: '.min.js'
                },
                noSource: true
            }))
            .pipe(gulp.dest(path))
    }

    function optimizeCss(path) {
        var src = [
            '!' + path + '/*.min.css',
            path + '/*.css'
        ];

        return gulp.src(src)
            .pipe($.groupCssMediaQueries())
            .pipe($.postcss([
                require('autoprefixer')({
                    browsers: [
                        "last 15 version"
                    ]
                }),
                require("postcss-inline-svg")(),
                require("postcss-svgo")()
            ]))
            // .pipe($.sourcemaps.init())
            .pipe($.cleanCss())
            // .pipe($.sourcemaps.write())
            .pipe($.rename({
                extname: '.min.css'
            }))
            .pipe(gulp.dest(path))
    }

    function modifyConfig(config, replaces) {
        Object.keys(config).map(function (key) {
            if (Array.isArray(config[key]))
                modifyConfig(config[key], replaces);
            else if (typeof config[key] === 'string') Object.keys(replaces).forEach(function (k) {
                config[key] = config[key].replace(k, replaces[k]);
            })
        });
    }

};
